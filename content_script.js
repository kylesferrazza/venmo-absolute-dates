const PAGES_TO_LOAD = 10;

function getTimeElem(storyId) {
  const headline = document.getElementById(`storyHeadlineId-${storyId}`);
  const timeParent = headline.nextSibling.nextSibling;
  return timeParent.firstChild.firstChild.firstChild;
}

function convertDate(dateStr) {
  const date = new Date(dateStr);
  return date.toLocaleDateString([], {
    year: "2-digit",
    month: "2-digit",
    day: "2-digit",
  });
}

function handleStory(story) {
  const {
    id,
    date,
  } = story;
  let timeElem;
  try {
    timeElem = getTimeElem(id);
  } catch(e) {
    return;
  }
  const newDateStr = convertDate(date);
  timeElem.textContent = newDateStr;
}

async function getAccountId() {
  const res = await fetch('https://account.venmo.com/api/user/identities');
  const arr = await res.json();
  if (arr.length < 1) {
    throw new Error("no identities");
  }
  return arr[0].externalId;
}

async function getStoriesRes(accountId, nextId = undefined) {
  let storiesURL = 'https://account.venmo.com/api/stories?feedType=me&externalId=' + accountId;
  if (nextId) {
    storiesURL += "&nextId=" + nextId;
  }
  const res = await fetch(storiesURL);
  const obj = await res.json();
  return obj;
}

async function main() {
  const accountId = await getAccountId();
  let allStories = [];
  let nextStoryId = undefined;
  for (let i = 0; i < PAGES_TO_LOAD; i++) {
    const res = await getStoriesRes(accountId, nextStoryId);
    if (!nextStoryId) {
      res.stories.forEach(handleStory);
    }
    nextStoryId = res.nextId;
    allStories = allStories.concat(res.stories);
  }
  function updateText() {
    allStories.forEach(handleStory);
  }
  updateText();
  setInterval(updateText, 1000);
}

main();
