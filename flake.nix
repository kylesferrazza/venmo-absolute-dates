{
  description = "venmo-absolute-dates";
  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "venmo-absolute-dates";
      buildInputs = with pkgs; [

      ];
    };
  };
}
